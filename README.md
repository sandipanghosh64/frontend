# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.7.

## Development server
Run `npm i` for install dependency
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Configuration
    | Configuration              | Description                            | 
    | baseUri                    | backend api                            |
    | pageSizeOptions            | pageSize options                       |
    | username                   | use in header to replay message                       |

### baseUri
- API url is configurable in src/environments. For testing, you can use
- https://damp-reef-53722.herokuapp.com/api

### Verification
- visit http://localhost:4200/
- start your tests by comparing the search results according to the postman collection requests examples.

### video
https://youtu.be/kLDTzHldEhA
