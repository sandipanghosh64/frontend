import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatSortModule,
} from '@angular/material';

import { AppComponent } from './app.component';

// routing module
import { AppRoutingModule } from './app-routing.module';

// pages
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ServiceSearchComponent } from './pages/serviceSearch/serviceSearch.component';

// components
import { HeaderComponent } from './components/header/header.component';

// services
import { DataService } from './services/data.service';

/**
 * Custom Toast Options
 */
export class CustomToastOption extends ToastOptions {
  positionClass = 'toast-bottom-right';
  newestOnTop = true;
  showCloseButton = true;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    ServiceSearchComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    AppRoutingModule,
    MatTableModule,
    MatNativeDateModule,
    MatPaginatorModule,
    FormsModule,
    MatCheckboxModule,
    MatSortModule,
    HttpModule,
    ToastModule.forRoot(),
  ],
  providers: [DataService, { provide: ToastOptions, useClass: CustomToastOption }],
  bootstrap: [AppComponent]
})
export class AppModule { }
