import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Sort } from '@angular/material';
import { DataService } from '../../services/data.service';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { ToastsManager } from 'ng2-toastr';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

import * as moment from 'moment';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-sd-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  displayedColumns = ['Select', 'Provider', 'Service', 'Version', 'Sync/Async', 'Type', 'UserID', 'Status', 'Replay', 'Timestamp'];
  dataSource: any;

  // select options
  serviceProviders: any [];
  serviceVersions: any [];
  messageTypes: any [];
  requestingSystems: any [];
  transmissionTypes: any [];
  statuses: any [];
  replayTypes = [{
    label: 'Yes',
    value: true,
  }, {
    label: 'No',
    value: false,
  }];

  providerId: number;
  service: string;
  messageTypeId: number;
  replayType: boolean;
  transmissionType: string;
  statusCd: string;
  requestingSystemId: number;
  requestingSystemMessageId: number;
  correlationId: string;
  versionId: number;
  startDt: number;
  endDt: number;
  userId: number;

  attrs: any[];

  sortOrder = '';
  sortBy = '';
  searchParams: any;
  directionMap = { asc: 'ASC', desc: 'DESC'};

  pageSizeOptions: number[] = environment.pageSizeOptions;

  constructor (private dataService: DataService,
               public toastr: ToastsManager,
               private vcr: ViewContainerRef) {
    this.providerId = null;
    this.messageTypeId = null;
    this.replayType = null;
    this.transmissionType = null;
    this.statusCd = null;
    this.requestingSystemId = null;
    this.requestingSystemMessageId = null;
    this.correlationId = null;
    this.versionId = null;
    this.startDt = null;
    this.service = null;
    this.toastr.setRootViewContainerRef(vcr);
    this.attrs = [];
    // init 3 rows of attr inputs
    for (let i = 0; i < 3; i += 1) {
      const attrs = [];
      for (let j = 0; j < 2; j += 1) {
        attrs.push({ value: '', name: '' });
      }
      this.attrs.push(attrs);
    }
  }

  ngOnInit () {
    this.getServiceProviders();
    this.getStatuses();
    this.getTransmissionType();
    this.getMessageTypes();
    this.getServiceVersions();
    this.getRequestingSystems();
  }

  /**
   * get service providers
   */
  getServiceProviders () {
    this.dataService.getServiceProviders().then(data => {
      this.serviceProviders = _.map(data, item => {
        return { value: item.serviceProviderId, label: item.name };
      });
    });
  }

  /**
   * get service versions
   */
  getServiceVersions () {
    this.dataService.getServiceVersions().then(data => {
      this.serviceVersions = _.map(data, item => {
        return { value: item.serviceVersionId, label: item.version };
      });
    });
  }

  /**
   * get message type
   */
  getMessageTypes () {
    this.dataService.getMessageTypes().then(data => {
      this.messageTypes = _.map(data, item => {
        return { value: item.messageTypeId, label: item.description };
      });
    });
  }

  /**
   * get requesting systems
   */
  getRequestingSystems () {
    this.dataService.getRequestingSystems().then(data => {
      this.requestingSystems = _.map(data, item => {
        return { value: item.requestingSystemId, label: item.name };
      });
    });
  }

  /**
   * get statuses
   */
  getStatuses () {
    this.dataService.getStatuses().then(data => {
      this.statuses = data;
      this.statuses = _.map(data, item => {
        return { value: item.statusCd, label: item.description };
      });
    });
  }

  /**
   * get transmission types
   */
  getTransmissionType () {
    this.dataService.getTransmissionType().then(data => {
      this.transmissionTypes = _.map(_.values(data), value => {
        return { value, label: value };
      });
    });
  }

  /**
   * clear the input values
   */
  clear() {
    this.providerId = null;
    this.messageTypeId = null;
    this.replayType = null;
    this.transmissionType = null;
    this.statusCd = null;
    this.requestingSystemId = null;
    this.requestingSystemMessageId = null;
    this.correlationId = null;
    this.versionId = null;
    this.service = null;
    this.userId = null;
    this.startDt = null;
    this.endDt = null;
    _.forEach(this.attrs, attrArray => {
      _.forEach(attrArray, attr => {
        attr.value = '';
        attr.name = '';
      });
    });
  }

  /**
   * get message type by id
   * @param {[type]} messageId [description]
   */
  getMessageTypeById(messageId) {
    const types = _.filter(this.messageTypes, item => item.value === messageId);
    return types[0] && types[0].label;
  }

  /**
   * change message data to table data
   * @param item
   * @return {{provider: any, service, version: any}}
   */
  transformTableData(item) {
    return {
      messageId: item.transactionId,
      provider: item.sdServiceProvider && item.sdServiceProvider.name,
      service: item.sdService && item.sdService.name,
      version: item.serviceVersion,
      transmissionType: item.transmissionType,
      type: this.getMessageTypeById(item.messageTypeId),
      userId: item.userId,
      status: item.statusCd,
      replay: item.replayMessage === true ? 'Yes' : 'No',
      timestamp: item.createdDate,
      selected: false,
    };
  }

  addDynamicAttr(name, value) {
    if (name !== '' && value !== '') {
      if (!this.searchParams['attributeSearchCriteria.attributes']) {
        this.searchParams['attributeSearchCriteria.attributes'] = [];
      }
      this.searchParams['attributeSearchCriteria.attributes'].push({ name, value });
    }
  }

  /**
   * prepare params to filter messages
   */
  prepareParams() {
    if (this.providerId) {
      this.searchParams['serviceProviderId'] = this.providerId;
    }
    if (this.versionId) {
      this.searchParams['serviceVersionId'] = this.versionId;
    }
    if (this.service && this.service !== '') {
      this.searchParams['name'] = this.service;
    }
    if (this.messageTypeId) {
      this.searchParams['messageTypeId'] = this.messageTypeId;
    }
    if (this.replayType) {
      this.searchParams['replayMessage'] = this.replayType;
    }
    if (this.userId) {
      this.searchParams['userId'] = this.userId;
    }
    if (this.requestingSystemId) {
      this.searchParams['requestingSystemId'] = this.requestingSystemId;
    }
    if (this.requestingSystemMessageId) {
      this.searchParams['requestingSystemMessageId'] = this.requestingSystemMessageId;
    }
    if (this.transmissionType) {
      this.searchParams['transmissionType'] = this.transmissionType;
    }
    if (this.statusCd) {
      this.searchParams['statusCd'] = this.statusCd;
    }
    if (this.startDt) {
      this.searchParams['startDate'] = moment(this.startDt).format('YYYY-MM-DD');
    }
    if (this.endDt) {
      this.searchParams['stopDate'] = moment(this.endDt).format('YYYY-MM-DD');;
    }
    if (this.correlationId && this.correlationId !== '') {
      this.searchParams['coorelationUuid'] = this.correlationId;
    }
    if (this.sortBy && this.sortOrder && this.sortBy !== '' && this.sortOrder !== '') {
      this.searchParams['sortBy'] = this.sortBy;
      this.searchParams['sortOrder'] = this.sortOrder;
    }

    _.forEach(this.attrs, attrArray => {
      _.forEach(attrArray, attr => {
        this.addDynamicAttr(attr.name, attr.value);
      });
    });
  }

  /**
   * search message
   */
  search() {
    this.searchParams = {};
    this.prepareParams();
    this.dataService.searchMessage(this.searchParams).then(data => {
      const array: MessageSchema[] = _.map(data.results, item => this.transformTableData(item));
      this.dataSource = new MessageDataSource(array, data.offset / data.limit, data.limit, data.total);
      if (data.total === 0) {
        this.toastr.info('No results found');
      }
    });
  }

  /**
   * set sort order
   * @param {Sort} sort
   */
  sortData(sort: Sort) {
    this.sortBy = sort.active;
    this.sortOrder = this.directionMap[sort.direction];
    if (this.dataSource) {
      this.search();
    }
  }

  /**
   * reset sort order
   */
  resetSortOrder() {
    this.sortBy = '';
    this.sortOrder = '';
    this.toastr.info('reset success');
  }

  /**
   * page change
   * @param event event of page change
   */
  onPaginateChange(event) {
    const offset = event.pageIndex * event.pageSize;
    const limit = event.pageSize;
    this.dataService.searchMessage(_.assign(this.searchParams, { offset, limit })).then(data => {
      const array: MessageSchema[] = _.map(data.results, item => this.transformTableData(item));
      this.dataSource = new MessageDataSource(array, data.offset / data.limit, data.limit, data.total);
    });
  }

  /**
   * replay message
   */
  replayMessage() {
    const messageIds = _.map(_.filter(this.dataSource.data, item => item.selected === true), item => item.messageId);
    if (messageIds.length == 0) {
      this.toastr.info('please select some message');
      return;
    }
    this.dataService.replayMessage({ messageIds }).then(() => {
      _.forEach(this.dataSource.data, item => {
        item.selected = false;
      });
      this.toastr.info('replay success');
    }).catch(error => {
      this.toastr.error('can not replay these message');
    });
  }
}

export interface MessageSchema {
  messageId: number;
  provider: string;
  service: string;
  version: number;
  transmissionType: string;
  type: string;
  userId: number;
  status: string;
  replay: string;
  timestamp: string;
  selected: boolean;
}


/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class MessageDataSource extends DataSource<any> {
  constructor(private data: any[],
              private pageIndex: number,
              private pageSize: number,
              private length: number) {
    super();
  }
  connect(): Observable<MessageSchema[]> {
    return Observable.of(this.data);
  }

  disconnect() {}
}
