import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Sort, MatSort } from '@angular/material';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { ToastsManager } from 'ng2-toastr';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-sd-dashboard',
  templateUrl: 'serviceSearch.component.html',
  styleUrls: ['serviceSearch.component.scss']
})

export class ServiceSearchComponent implements OnInit {
  displayedColumns = ['Provider', 'Service', 'Version'];
  dataSource: any;
  serviceProviders: any [];
  serviceVersions: any [];

  providerId: number;
  service: string;
  versionId: number;
  sortOrder = '';
  sortBy = '';
  searchParams: any;

  directionMap = { asc: 'ASC', desc: 'DESC'};

  pageSizeOptions: number[] = environment.pageSizeOptions;

  constructor (private dataService: DataService,
               public toastr: ToastsManager,
               private vcr: ViewContainerRef) {
    this.providerId = null;
    this.versionId = null;
    this.service = null;
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit () {
    this.getServiceProviders();
    this.getServiceVersions();
  }

  /**
   * get service providers
   */
  getServiceProviders () {
    this.dataService.getServiceProviders().then(data => {
      this.serviceProviders = _.map(data, item => {
        return { value: item.serviceProviderId, label: item.name };
      });
    });
  }

  /**
   * get service versions
   */
  getServiceVersions () {
    this.dataService.getServiceVersions().then(data => {
      this.serviceVersions = _.map(data, item => {
        return { value: item.serviceVersionId, label: item.version };
      });
    });
  }

  /**
   * change service data to table data
   * @param item
   * @return {{provider: any, service, version: any}}
   */
  transfromTableData(item) {
    return {
      provider: item.sdServiceProvider && item.sdServiceProvider.name,
      service: item.name,
      version: item.sdServiceVersion && item.sdServiceVersion.version,
    };
  }

  /**
   * prepare params
   */
  prepareParams() {
    if (this.providerId) {
      this.searchParams['serviceProviderId'] = this.providerId;
    }
    if (this.service && this.service !== '') {
      this.searchParams['name'] = this.service;
    }
    if (this.versionId) {
      this.searchParams['serviceVersionId'] = this.versionId;
    }
    if (this.sortBy && this.sortOrder && this.sortBy !== '' && this.sortOrder !== '') {
      this.searchParams['sortBy'] = this.sortBy;
      this.searchParams['sortOrder'] = this.sortOrder;
    }
  }

  /**
   * search service
   */
  search() {
    this.searchParams = {};
    this.prepareParams();
    this.dataService.searchService(this.searchParams).then(data => {
      const array: ServiceSchema[] = _.map(data.results, item => this.transfromTableData(item));
      this.dataSource = new ServiceDataSource(array, data.offset / data.limit, data.limit, data.total);
      if (data.total === 0) {
        this.toastr.info('No results found');
      }
    });
  }

  /**
   * clear search params
   */
  clear() {
    this.providerId = null;
    this.service = null;
    this.versionId = null;
  }
  /**
   * reset sort order
   */
  resetSortOrder() {
    this.sortBy = '';
    this.sortOrder = '';
    this.toastr.info('reset success');
  }

  /**
   * set sort order
   * @param {Sort} sort
   */
  sortData(sort: Sort) {
    this.sortBy = sort.active;
    this.sortOrder = this.directionMap[sort.direction];
    this.search();
  }

  /**
   * page change
   * @param event event of page change
   */
  onPaginateChange(event) {
    const offset = event.pageIndex * event.pageSize;
    const limit = event.pageSize;
    this.dataService.searchService(_.assign(this.searchParams, { offset, limit })).then(data => {
      const array: ServiceSchema[] = _.map(data.results, item => this.transfromTableData(item));
      this.dataSource = new ServiceDataSource(array, data.offset / data.limit, data.limit, data.total);
    });
  }

}

export interface ServiceSchema {
  provider: string;
  service: string;
  version: string;
}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class ServiceDataSource extends DataSource<any> {
  constructor(private data: any[],
              private pageIndex: number,
              private pageSize: number,
              private length: number) {
    super();
  }
  connect(): Observable<ServiceSchema[]> {
    return Observable.of(this.data);
  }

  disconnect() {}
}
