export const environment = {
  production: true,
  baseUri: 'https://damp-reef-53722.herokuapp.com/api',
  username: 'Michael Lee',
  pageSizeOptions: [5, 10, 20, 50]
};
